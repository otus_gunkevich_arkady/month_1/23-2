﻿namespace ConsoleMenu.Components
{
    public class Table
    {
        public List<string> GetDouble(Dictionary<string, double> table, int startIndex = 1, string titleKey = "Описание", string titleValue = "Значение", bool total = false)
        {
            if (table.Count == 0) return new List<string>();
            var result = new List<string>();
            int maxWidthindex = (table.Count + 1 + startIndex).ToString().Length;
            int maxWidthKey = Math.Max(table.Select(s => s.Key.Length).Max(), titleKey.Length);
            int maxWidthValue = Math.Max(table.Sum(s => s.Value).ToString().Length, titleValue.Length);

            result.Add($"╔{"═".Repeat(maxWidthindex)}╦{"═".Repeat(maxWidthKey)}╦{"═".Repeat(maxWidthValue)}╗");
            result.Add($"║#{" ".Repeat(maxWidthindex - 1)}║{titleKey}{" ".Repeat(maxWidthKey - titleKey.Length)}║{titleValue}{" ".Repeat(maxWidthValue - titleValue.Length)}║");

            foreach (var element in table.Select((item, index) => (item, index)))
            {
                result.Add($"╠{"═".Repeat(maxWidthindex)}╦{"═".Repeat(maxWidthKey)}╬{"═".Repeat(maxWidthValue)}╣");
                result.Add($"║{element.index + startIndex}{" ".Repeat(maxWidthindex - (element.index + startIndex).ToString().Length)}║{element.item.Key}{" ".Repeat(maxWidthKey - element.item.Key.Length)}║{element.item.Value}{" ".Repeat(maxWidthValue - element.item.Value.ToString().Length)}║");
            }

            if (total)
            {
                result.Add($"╠{"═".Repeat(maxWidthindex)}╦{"═".Repeat(maxWidthKey)}╬{"═".Repeat(maxWidthValue)}╣");
                result.Add($"║{" ".Repeat(maxWidthindex)}║Итого{" ".Repeat(maxWidthKey - "Итого".Length)}║{table.Sum(s => s.Value)}{" ".Repeat(maxWidthValue - table.Sum(s => s.Value).ToString().Length)}║");
            }

            result.Add($"╚{"═".Repeat(maxWidthindex)}╩{"═".Repeat(maxWidthKey)}╩{"═".Repeat(maxWidthValue)}╝");
            return result;
        }
    }
}
