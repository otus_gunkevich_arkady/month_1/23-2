﻿using ConsoleMenu;
using ConsoleMenu.Components;
using System.Diagnostics;

namespace DotnetDev.Homework._23_2
{
    public class ReadFiles : Menu
    {
        private List<TableTimer> _tableTimer;
        private class TableTimer
        {
            public string Name { get; set; }
            public double Value { get; set; }
            public TableTimer(string name, double value) { Name = name; Value = value; }
        }
        private int _page { get; set; }
        private int _skip { get; set; }
        public ReadFiles()
        {
            _page = 0;
            _skip = 10;
            _buttons = new List<Button>
            {
                new Button("Прочитать 3 файла.", () => CountingSpace(GroupTotalThreads.Files, 3)),
                new Button("Прочитать все файлы [группировка по файлам]", () => CountingSpace(GroupTotalThreads.Files)),
                new Button("Прочитать все файлы [группировка по потокам]", () => CountingSpace(GroupTotalThreads.Thread)),
                new Button("[*] Назад", () => _exit = true)
            };
        }
        private void UpdateText()
        {
            _text = new List<string>();
            if (_tableTimer is not null)
            {
                _text.Add($"Для перелистывания используйте стрелки влево и вправо");
                _text.Add($"Стрелва влево <<{_page + 1}/{_tableTimer.Count / _skip + 1}>> Стелка вправо");
                _text.AddRange(new Table().GetDouble(_tableTimer.Skip(_skip * _page).Take(_skip).ToDictionary(s => s.Name, s => s.Value), _page * _skip + 1, "Файл (кол-во пробелов)", "Время в секундах"));
            }
        }
        public override void Left()
        {
            if (_page > 0) _page--;
            UpdateText();
        }
        public override void Right()
        {
            if (_page < _tableTimer.Count / _skip) _page++;
            UpdateText();
        }
        private void CountingSpace(GroupTotalThreads group, int count = 0)
        {
            this._isBlock = true;
            _tableTimer = new List<TableTimer>();
            _page = 0;

            List<FileInfo> listFile = Directory.GetFiles(SETTING.PATH_FILES).Select(s => new FileInfo(s)).ToList();
            if (count > 0) listFile = listFile.Take(count).ToList();
            int countThread = Process.GetCurrentProcess().Threads.Count;
            countThread = (listFile.Count() < countThread ? listFile.Count() : countThread);
            int stepSkip = listFile.Count() / countThread;

            Stopwatch _stopwatch = Stopwatch.StartNew();
            using (CountdownEvent countEvent = new CountdownEvent(countThread))
            {
                for (int i = 0; i < countThread; i++)
                {
                    var list = listFile.Skip(stepSkip * i).Take(stepSkip).ToList();
                    int index = i;
                    ThreadPool.QueueUserWorkItem(
                        new WaitCallback(s =>
                        {
                            if (group == GroupTotalThreads.Files)
                                CountingSpaceInFiles(list).Wait();
                            else if (group == GroupTotalThreads.Thread)
                                CountingSpaceInFiles(list, index).Wait();

                            countEvent.Signal();
                        }));
                }
                countEvent.Wait();
            }
            _stopwatch.Stop();
            _tableTimer.Add(new TableTimer($"Потоков {countThread}. Файлов {listFile.Count}", new TimeSpan(_stopwatch.ElapsedTicks).TotalSeconds));

            this._isBlock = false;
            UpdateText();
            Write();
        }
        private async Task CountingSpaceInFiles(List<FileInfo> listFile)
        {
            foreach (FileInfo file in listFile)
            {
                int countSpace = 0;
                string line;
                Stopwatch _stopwatch = Stopwatch.StartNew();
                using (StreamReader stream = new StreamReader(file.FullName))
                {
                    while ((line = stream.ReadLine()) != null)
                    {
                        countSpace += CountingSpaceInText(line);
                    }
                }
                _stopwatch.Stop();

                _tableTimer.Add(new TableTimer($"Пробелов {countSpace} Файл {file.Name}", new TimeSpan(_stopwatch.ElapsedTicks).TotalSeconds));
            }
        }
        private async Task CountingSpaceInFiles(List<FileInfo> listFile, int index)
        {
            int countSpace = 0;
            Stopwatch _stopwatch = Stopwatch.StartNew();
            foreach (FileInfo file in listFile)
            {
                string line;
                using (StreamReader stream = new StreamReader(file.FullName))
                {
                    while ((line = stream.ReadLine()) != null)
                    {
                        countSpace += CountingSpaceInText(line);
                    }
                }
            }
            _stopwatch.Stop();
            _tableTimer.Add(new TableTimer($"Поток {index} Файлов {listFile.Count()} Пробелов {countSpace}", new TimeSpan(_stopwatch.ElapsedTicks).TotalSeconds));
        }
        private int CountingSpaceInText(string text) => text.Count(s => s == ' ');
    }
}
