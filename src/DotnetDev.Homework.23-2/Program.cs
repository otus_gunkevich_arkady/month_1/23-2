﻿using ConsoleMenu;
using DotnetDev.Homework._23_2;

public class Program
{
    private static void Main(string[] args) => new Visual(new Start()).Show();
}