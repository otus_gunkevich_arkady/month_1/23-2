﻿using ConsoleMenu;
using System.Diagnostics;

namespace DotnetDev.Homework._23_2
{
    public class GenerateFiles : Menu
    {
        private string _symbols = " abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ \r";
        private Random _rnd = new Random();
        private Stopwatch _stopwatch;
        private int _countFiles = 100;
        private int _countSimbol = 500000;
        public GenerateFiles()
        {
            _buttons = new List<Button>
            {
                new Button("Сгенерировать файлы", async () => await Generate()),
                new Button("Удалить все файлы с папки [аккуратно]", () => DeleteFiles()),
                new Button("[*] Назад", () => _exit = true)
            };
            UpdateText();
        }
        private void UpdateText()
        {
            _text = new List<string>()
            {
                $"[Настройки] - Количество файлов {_countFiles}",
                $"[Настройки] - Количество символов в файле {_countSimbol}"
            };
        }
        private string GetRandomString(int lenght) => new string(Enumerable.Repeat(_symbols, lenght).Select(s => s[_rnd.Next(s.Length)]).ToArray());
        public async Task Generate()
        {
            this._isBlock = true;
            UpdateText();
            _stopwatch = Stopwatch.StartNew();
            for (int i = 0; i < _countFiles; i++)
            {
                await File.WriteAllTextAsync(Path.Combine(SETTING.PATH_FILES, $"{Guid.NewGuid()}.txt"), GetRandomString(_countSimbol));
            }
            _stopwatch.Stop();
            _text.Add($"Файлы сгенерированы за {new TimeSpan(_stopwatch.ElapsedTicks).TotalSeconds} секунд");
            Write();
            this._isBlock = false;
        }
        public void DeleteFiles()
        {
            this._isBlock = true;
            UpdateText();
            Directory.GetFiles(SETTING.PATH_FILES).AsParallel().ForAll(file => { File.Delete(file); });
            _text.Add("Файлы удалены!");
            this._isBlock = false;
        }
    }
}
