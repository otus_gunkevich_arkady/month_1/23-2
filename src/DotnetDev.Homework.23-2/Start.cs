﻿using ConsoleMenu;

namespace DotnetDev.Homework._23_2
{
    public class Start : Menu
    {
        public Start()
        {
            if (!Directory.Exists(SETTING.PATH_FILES))
            {
                Directory.CreateDirectory(SETTING.PATH_FILES);
            }
            UpdateButton();
            UpdateText();
        }
        private void UpdateButton()
        {
            _buttons = new List<Button>();
            _buttons.Add(new Button("Генератор файлов", () => { new Visual(new GenerateFiles()).Show(); UpdateButton(); }));
            if (Directory.GetFiles(SETTING.PATH_FILES).Count() > 0)
            {
                _buttons.Add(new Button("Чтение файлов", () => new Visual(new ReadFiles()).Show()));
            }
            _buttons.Add(new Button("[*] Выйти", () => _exit = true));
            if (this._isLoad) _menu.LoadButton(_buttons);
        }
        private void UpdateText()
        {
            _text = new List<string>
                    {
                        "",
                        "",
                        "Гункевич Аркадий Игоревич | OTUS",
                        "ДЗ: Параллельное считывание файлов"
                    };
        }
    }
}
